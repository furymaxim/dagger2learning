package com.furymaxim.dagger2examples.dagger

import com.furymaxim.dagger2examples.car.Rims
import com.furymaxim.dagger2examples.car.Tires
import com.furymaxim.dagger2examples.car.Wheels
import dagger.Module
import dagger.Provides


@Module
class WheelsModule {


    @Provides
    fun provideRims(): Rims {
        return Rims()
    }

    @Provides
    fun provideTires(): Tires {
        val tires = Tires()
        tires.inflate()
        return Tires()
    }

    @Provides
    fun provideWheels(rims: Rims, tires: Tires): Wheels {
        return Wheels(rims, tires)
    }

}