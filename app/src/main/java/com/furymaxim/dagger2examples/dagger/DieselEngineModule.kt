package com.furymaxim.dagger2examples.dagger

import com.furymaxim.dagger2examples.car.DieselEngine
import com.furymaxim.dagger2examples.car.Engine
import dagger.Binds
import dagger.Module
import dagger.Provides


@Module
class DieselEngineModule(var horsePower:Int) {


    @Provides
    fun provideHorsePower(): Int{
        return horsePower
    }

    @Provides
    fun provideEngine(engine: DieselEngine): Engine{
        return engine
    }


}

