package com.furymaxim.dagger2examples.dagger

import com.furymaxim.dagger2examples.MainActivity
import com.furymaxim.dagger2examples.car.Car
import dagger.BindsInstance
import dagger.Component
import dagger.Subcomponent
import javax.inject.Named

@PerActivity
//@Component (dependencies = [AppComponent::class],modules = [WheelsModule::class, PetrolEngineModule::class])
//@Subcomponent (modules = [WheelsModule::class, DieselEngineModule::class])
@Subcomponent (modules = [WheelsModule::class, PetrolEngineModule::class])
interface ActivityComponent {

    //fun getCar(): Car

    // if we have a constructor then fields and methods will be injected automatically, else -> manually (calling inject
    // on a component)

    // can't declare argument as a superclass
    fun inject(mainActivity: MainActivity)

   /* @Subcomponent.Builder
    interface Builder{

        fun horsePower(@BindsInstance @Named("horse power") horsePower: Int): Builder

        @BindsInstance
        fun engineCapacity(@Named("engine capacity") engineCapacity: Int): Builder

       // fun appComponent(component: AppComponent): Builder // needed for component dependencies

        fun build(): ActivityComponent
    }
    */


     @Subcomponent.Factory
     interface Factory{

         fun create(@BindsInstance @Named("horse power")horsePower: Int,
                    @BindsInstance @Named("engine capacity") engineCapacity: Int): ActivityComponent
     }



}