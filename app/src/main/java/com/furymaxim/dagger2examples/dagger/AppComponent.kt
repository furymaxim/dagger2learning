package com.furymaxim.dagger2examples.dagger

import com.furymaxim.dagger2examples.car.Driver
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DriverModule::class])
interface AppComponent {

    //fun getDriver(): Driver

    //fun getActivityComponent(dieselEngineModule: DieselEngineModule):ActivityComponent

    //fun getActivityComponentBuilder():ActivityComponent.Builder

    fun getActivityComponentFactory():ActivityComponent.Factory


    @Component.Factory
    interface Factory{

        fun create(driverModule: DriverModule): AppComponent
    }

}