package com.furymaxim.dagger2examples.dagger

import com.furymaxim.dagger2examples.car.Driver
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DriverModule(var name: String) {



    @Singleton
    @Provides
    fun provideDriver(): Driver{
        return Driver(name)
    }
}