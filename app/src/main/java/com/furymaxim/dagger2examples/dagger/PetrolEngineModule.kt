package com.furymaxim.dagger2examples.dagger

import com.furymaxim.dagger2examples.car.Engine
import com.furymaxim.dagger2examples.car.PetrolEngine
import dagger.Binds
import dagger.Module


@Module
abstract class PetrolEngineModule {

    @Binds
    abstract fun bindEngine(engine: PetrolEngine): Engine

    //we can't create an instance, but we can add a method to companion
}
/*
@Module
class PetrolEngineModule {

    @Provides
    fun provideEngine(engine: PetrolEngine): Engine{
        return engine
    }
}*/
