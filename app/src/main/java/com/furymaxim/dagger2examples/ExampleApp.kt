package com.furymaxim.dagger2examples

import android.app.Application
import com.furymaxim.dagger2examples.dagger.AppComponent
import com.furymaxim.dagger2examples.dagger.DaggerAppComponent
import com.furymaxim.dagger2examples.dagger.DriverModule

class ExampleApp: Application(){

    private lateinit var component: AppComponent

    override fun onCreate() {
        super.onCreate()

      component = DaggerAppComponent.factory().create(DriverModule("Max"))
    }

    fun getAppComponent(): AppComponent {
        return component
    }
}