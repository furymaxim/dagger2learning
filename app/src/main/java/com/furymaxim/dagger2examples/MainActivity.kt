package com.furymaxim.dagger2examples

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.furymaxim.dagger2examples.car.Car
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var car1: Car

    @Inject
    lateinit var car2: Car


    // private lateinit var car: Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    /*    val component = DaggerActivityComponent.builder()
            .horsePower(120)
            .engineCapacity(1500)
            .appComponent((application as ExampleApp).getAppComponent())
            .build()
            
     */

      /*  val component = (application as ExampleApp).getAppComponent()
            //.getActivityComponent(DieselEngineModule(150))
            .getActivityComponentBuilder()
            .engineCapacity(1500)
            .horsePower(190)
            .build()

       */

        val component = (application as ExampleApp).getAppComponent()
            .getActivityComponentFactory()
            .create(150,2000)








        component.inject(this)

        //car = component.getCar()

        car1.drive()
        car2.drive()

    }
}
