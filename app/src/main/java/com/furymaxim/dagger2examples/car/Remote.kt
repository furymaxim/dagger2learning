package com.furymaxim.dagger2examples.car

import android.util.Log
import com.furymaxim.dagger2examples.car.Car
import javax.inject.Inject

class Remote @Inject constructor(){

    fun setListener(car: Car){
        Log.d(Car.TAG,"Remote connected")
    }

}