package com.furymaxim.dagger2examples.car

import android.util.Log
import javax.inject.Inject

class DieselEngine @Inject constructor(var horsePower: Int): Engine {

    override fun start() {
        Log.d(Car.TAG, "Diesel engine started. Horsepower: $horsePower")
    }
}