package com.furymaxim.dagger2examples.car

import com.furymaxim.dagger2examples.car.Rims
import com.furymaxim.dagger2examples.car.Tires


// we do not own this class!!
class Wheels constructor(var rims: Rims, var tires: Tires){

}