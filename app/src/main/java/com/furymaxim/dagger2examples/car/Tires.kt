package com.furymaxim.dagger2examples.car

import android.util.Log
import com.furymaxim.dagger2examples.car.Car

class Tires {

    fun inflate(){
        Log.d(Car.TAG, "Tires inflated")
    }
}