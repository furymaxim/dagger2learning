package com.furymaxim.dagger2examples.car

import android.util.Log
import com.furymaxim.dagger2examples.dagger.PerActivity
import javax.inject.Inject

@PerActivity
class Car @Inject constructor(val engine: Engine, val wheels: Wheels, val driver: Driver) {

    //@Inject lateinit var engine: Engine

    companion object{

        const val TAG = "Car"
    }

    @Inject
    fun enableRemote(remote: Remote){
        remote.setListener(this)
    }

    fun drive(){
        engine.start()
        Log.d(TAG,"$driver ${driver.name} drives $this")
    }
}