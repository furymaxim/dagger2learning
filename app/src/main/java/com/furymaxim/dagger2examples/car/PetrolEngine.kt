package com.furymaxim.dagger2examples.car

import android.util.Log
import com.furymaxim.dagger2examples.car.Car
import com.furymaxim.dagger2examples.car.Engine
import javax.inject.Inject
import javax.inject.Named

class PetrolEngine @Inject constructor(@Named("horse power") var horsePower: Int,@Named("engine capacity")  var engineCapacity: Int): Engine {


    override fun start() {
        Log.d(Car.TAG, "Petrol engine started. \nHorsepower: $horsePower\n Engine capacity: $engineCapacity")
    }
}