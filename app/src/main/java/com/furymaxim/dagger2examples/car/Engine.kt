package com.furymaxim.dagger2examples.car


interface Engine{

    fun start()
}