package com.furymaxim.dagger2examples.car

import javax.inject.Inject
import javax.inject.Singleton



/* we don't own this class
@Singleton
class Driver @Inject constructor() {
}*/

class Driver(val name: String)